# syntax=docker/dockerfile:1
FROM ubuntu:18.04
LABEL MAINTAINER Vitaliy Mikhnevych

RUN apt-get update \
    && apt-get install -y locales \
    && locale-gen en_US.UTF-8

ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8 
ENV TZ=UTC

RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
RUN apt-get update \
    && apt-get install -y cron nginx curl zip unzip vim netcat jq git software-properties-common supervisor
    #\
    #&& add-apt-repository -y ppa:ondrej/php \
    #&& apt-get update
RUN apt-get install -y php7.2-fpm php7.2-cli php7.2-gd php7.2-mysql\
       php7.2-mbstring php7.2-xml php7.2-curl php7.2-intl php7.2-xsl php7.2-soap\
    && php -r "readfile('http://getcomposer.org/installer');" | php -- --install-dir=/usr/bin/ --filename=composer \
    && mkdir /run/php \
    && apt-get remove -y --purge software-properties-common \
    && apt-get -y autoremove \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

#RUN ln -sf /dev/stdout /var/log/nginx/access.log \
#    && ln -sf /dev/stderr /var/log/nginx/error.log
RUN chmod 0777 /var/log/supervisor

COPY geoip/ /etc/nginx/geoip/
#COPY conf/nginx.conf /etc/nginx/nginx.conf
COPY conf/site.conf /etc/nginx/sites-available/default

#COPY conf/php-fpm.conf /etc/php/7.0/fpm/php-fpm.conf
COPY conf/php.ini /etc/php/7.2/fpm/php.ini

EXPOSE 80

COPY conf/supervisord.conf /etc/supervisor/conf.d/supervisord.conf
COPY conf/cron.conf /etc/supervisor/conf.d/cron.conf
#USER www-data

CMD ["/usr/bin/supervisord"]
